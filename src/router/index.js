import { createRouter, createWebHistory } from "vue-router";

const routes = [
	{
		path: "/",
		name: "MovieList",
		component: () => import("../views/List.vue"),
	},
	{
		path: "/movie/:id",
		name: "Movie",
		component: () => import("../views/Show.vue"),
	},
];

const router = createRouter({
	history: createWebHistory(),
	routes,
});

export default router;
