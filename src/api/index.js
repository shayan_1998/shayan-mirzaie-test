import axios from "axios";
const API_URL = "";
// api collections
import Movies from "./movies";

export default class Api {
	instance = null;
	http;
	services = new Map();

	constructor() {
		this.setupAxios();
		this.setupServices();
	}

	setupAxios() {
		this.http = axios.create({
			baseURL: API_URL,
		});
		this.http.interceptors.request.use(
			async function (config) {
				config.headers.Authorization = `Bearer ${await localStorage.getItem(
					"token"
				)}`;
				config.headers.Accept = "application/json";
				config.params = {
					...config.params,
					api_key: "f62f750b70a8ef11dad44670cfb6aa57",
				};
				return config;
			},
			function (error) {
				return Promise.reject(error);
			}
		);

		let localHttp = this.http;
		this.http.interceptors.response.use(
			function (response) {
				//2**
				console.log(
					`%c ${response.status} -> ${response.config.url}`,
					"color: #77C146",
					response
				);
				return response;
			},
			async function (error) {
				//4** OR 5**
				let { status, config } = error.response;
				console.log(
					`%c ${status} -> ${config.url}`,
					"color: #d64141",
					error.response
				);

				return Promise.reject(error);
			}
		);
	}

	//add services to api services map
	setupServices() {
		this.services.set("Movies", new Movies(this.http));
	}

	//get an singleton instance of api class
	static getInstance() {
		if (this.instance != null) {
			return this.instance;
		} else {
			this.instance = new Api();
			return this.instance;
		}
	}

	//get access to each service
	MoviesService() {
		return this.services.get("Movies");
	}
}
