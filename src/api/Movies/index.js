import moment from "moment";

export default class Movies {
	http;

	constructor(http) {
		this.http = http;
	}
	async movieList(page = 1, date) {
		let params = { page };
		if (date.length) {
			params = {
				...params,
				"primary_release_date.gte": moment(date[0]).format(
					"YYYY-MM-DD"
				),
				"primary_release_date.lte": moment(date[1]).format(
					"YYYY-MM-DD"
				),
			};
		}
		return await this.http.get(
			`https://api.themoviedb.org/3/discover/movie`,
			{
				params,
			}
		);
	}

	async movieDetail(movie_id) {
		return await this.http.get(
			`https://api.themoviedb.org/3/movie/${movie_id}`
		);
	}

	async movieCredits(movie_id) {
		return await this.http.get(
			`https://api.themoviedb.org/3/movie/${movie_id}/credits`
		);
	}

	async genreList() {
		return await this.http.get(
			`https://api.themoviedb.org/3/genre/movie/list`
		);
	}
}
