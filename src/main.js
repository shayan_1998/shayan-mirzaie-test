import { createApp } from "vue";
import App from "./App.vue";
import Router from "./router";
import VCalendar from "v-calendar";
import delimiter from "./plugins/delimiter.js";
import "./index.css";
import { library } from "@fortawesome/fontawesome-svg-core";
import { faArrowLeft, faCalendar } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import Vue3PersianDatetimePicker from "vue3-persian-datetime-picker";

library.add(faArrowLeft);
library.add(faCalendar);

const app = createApp(App);
app.component("font-awesome-icon", FontAwesomeIcon);
app.use(Router);
app.use(delimiter);
app.use(VCalendar, {});
app.component("date-picker", Vue3PersianDatetimePicker);
app.mount("#app");
