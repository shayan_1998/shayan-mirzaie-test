export default {
	install: (app, options) => {
		app.config.globalProperties.$delimiter = function (price = 0) {
			let newPrice = price
				.toString()
				.replace(/\B(?=(\d{3})+(?!\d))/g, "'");
			return newPrice;
		};
	},
};
